//
//  Hero.h
//  DoTa_Index
//
//  Created by Andre Ferreira dos Santos on 25/03/15.
//  Copyright (c) 2015 Andre Ferreira dos Santos. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Hero : NSObject

@property NSString* nome;
@property NSString* str;
@property NSString* strGrow;
@property NSString* agi;
@property NSString* agiGrow;
@property NSString* inte;
@property NSString* inteGrow;
@property NSString* moveSpeed;
@property NSString* atkRange;
@property NSString* colSize;
@property NSString* img_Path;

-(void)configure: (NSMutableArray*) ref;

@end
