//
//  PerfilViewController.m
//  DoTa_Index
//
//  Created by Andre Ferreira dos Santos on 24/03/15.
//  Copyright (c) 2015 Andre Ferreira dos Santos. All rights reserved.
//

#import "PerfilViewController.h"

#import "DataManager.h"

@interface PerfilViewController ()

@end

@implementation PerfilViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //_hero = [[[DataManager getDataManager]dict]objectForKey:@"showHero"];
    
    NSString *pathIMG = _hero.img_Path;
    
    pathIMG = [pathIMG substringToIndex:[pathIMG length]-1];
    
    pathIMG = [NSString stringWithFormat:@"%@.png",pathIMG];
    
    _heroName.text = _hero.nome;
    _heroImage.image = [UIImage imageNamed:pathIMG];
    
    _labStr.text = [NSString stringWithFormat:@"%@ + %@",_hero.str, _hero.strGrow];
    _labAgi.text = [NSString stringWithFormat:@"%@ + %@",_hero.agi, _hero.agiGrow];
    _labInt.text = [NSString stringWithFormat:@"%@ + %@",_hero.inte,_hero.inteGrow];
    
    _labatkRange.text = [NSString stringWithFormat:@"Attack Range: %@",_hero.atkRange];
    _labColSize.text = [NSString stringWithFormat:@"Collision Size: %@",_hero.colSize];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)voltar:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
