//
//  DataManager.h
//  Quizz
//
//  Created by Andre Ferreira dos Santos on 17/03/15.
//  Copyright (c) 2015 Andre Ferreira dos Santos. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataManager : NSObject

@property NSMutableDictionary *dict;

+(id)getDataManager;

-(NSString *)LoadStringFromFile:(NSString *)file useBundle:(BOOL)bundle;

-(NSMutableArray *)getHeroes;

@end
