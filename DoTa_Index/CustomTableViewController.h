//
//  CustomTableViewController.h
//  DoTa_Index
//
//  Created by Andre Ferreira dos Santos on 26/03/15.
//  Copyright (c) 2015 Andre Ferreira dos Santos. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface CustomTableViewController : UITableViewController <UISearchResultsUpdating,UISearchBarDelegate, AVAudioPlayerDelegate> {
    AVAudioPlayer *allPick;
    AVAudioPlayer  *music;
}

@property NSMutableArray* heroes;
@property NSArray *resultHeroes;

@property NSMutableDictionary *heroIndex;
@property NSArray *heroKeys;

@property NSInteger heroAtual;

@property UISearchController *buscaHero;

-(void)buscar:(NSString *)_text;

-(void)initIndex;

-(void)playAllPick;


@end
