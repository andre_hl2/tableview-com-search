//
//  CustomCellTableViewCell.m
//  DoTa_Index
//
//  Created by Andre Ferreira dos Santos on 24/03/15.
//  Copyright (c) 2015 Andre Ferreira dos Santos. All rights reserved.
//

#import "CustomCellTableViewCell.h"

@implementation CustomCellTableViewCell

- (void)awakeFromNib {
    // Initialization code
    //UIImageView *img = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"HUD_frame.png"]];
    //self.backgroundView = img;
    self.backgroundColor = [UIColor clearColor];
}

-(void)configure
{
    NSString *pathIMG = _myHero.img_Path;
    pathIMG = [pathIMG substringToIndex:[pathIMG length]-1];
    
    pathIMG = [NSString stringWithFormat:@"%@.png",pathIMG];
    
    _labelHeroName.text = _myHero.nome;
    _imageHero.image = [UIImage imageNamed:pathIMG];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
