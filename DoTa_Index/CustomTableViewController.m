//
//  CustomTableViewController.m
//  DoTa_Index
//
//  Created by Andre Ferreira dos Santos on 26/03/15.
//  Copyright (c) 2015 Andre Ferreira dos Santos. All rights reserved.
//

#import "CustomTableViewController.h"
#import "PerfilViewController.h"
#import "Hero.h"

#import "CustomCellTableViewCell.h"

#import "DataManager.h"

@interface CustomTableViewController () <UISearchResultsUpdating, UISearchBarDelegate>

@end

@implementation CustomTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //SOM ALL PICK
    NSString *path = [[NSBundle mainBundle]pathForResource:@"all_pick" ofType:@"mp3"];
    
    allPick = [[AVAudioPlayer alloc]initWithContentsOfURL:[NSURL fileURLWithPath:path] error:NULL];
    allPick.delegate = self;
    allPick.numberOfLoops = 0;
    
    [self performSelector:@selector(playAllPick) withObject:self afterDelay:1.0];
    
    //MUSICA FUNDO
    path = [[NSBundle mainBundle]pathForResource:@"musica" ofType:@"mp3"];
    
    music = [[AVAudioPlayer alloc]initWithContentsOfURL:[NSURL fileURLWithPath:path] error:NULL];
    music.delegate = self;
    music.numberOfLoops = -1;
    music.volume = 0.6;
    [music play];
    
    
    //BUSCA
    _buscaHero = [[UISearchController alloc]initWithSearchResultsController:nil];
    _buscaHero.searchResultsUpdater = self;
    _buscaHero.dimsBackgroundDuringPresentation = NO;
    _buscaHero.searchBar.delegate = self;
    [_buscaHero.searchBar sizeToFit];
    self.tableView.tableHeaderView = _buscaHero.searchBar;
    self.definesPresentationContext = YES;
    
    
    //DADOS DAS TABELAS
    NSMutableArray *res = [[DataManager getDataManager]getHeroes];
    
    _heroes = [[NSMutableArray alloc]init];
    _resultHeroes = [[NSMutableArray alloc]init];
    
    //ORGANIZAGAO DOS HEROES
    for( int i=0;i<[res count];i++)
    {
        Hero *hero = [[Hero alloc]init];
        [hero configure:[res objectAtIndex:i]];
        [_heroes setObject:hero atIndexedSubscript:i];
    }

    [self initIndex];
}

-(void)playAllPick
{
    [allPick play];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) buscar:(NSString *)texto {
    NSPredicate *busca = [NSPredicate predicateWithFormat:@"nome contains[c] %@",texto];
    _resultHeroes = [_heroes filteredArrayUsingPredicate:busca];
}

-(void)initIndex
{
    _heroIndex = [[NSMutableDictionary alloc]init];
    
    for( Hero *hero in _heroes)
    {
        NSString *first =[[hero.nome substringToIndex:1]uppercaseString];
        
        NSMutableArray *heroes = [_heroIndex objectForKey:first];
        
        if(!heroes)
        {
            heroes = [NSMutableArray array];
            [_heroIndex setObject:heroes forKeyedSubscript:first];
        }
        [heroes addObject:hero];
    }
    _heroKeys = [[_heroIndex allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    if(_buscaHero.active)
        return 1;
    else
        return [_heroKeys count];
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(_buscaHero.active)
        return nil;
    else
        return [_heroKeys objectAtIndex:section];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if(_buscaHero.active)
    {
        return [_resultHeroes count];
    }
    else
    {
        NSString *str_section = [_heroKeys objectAtIndex:section];
        return [[_heroIndex objectForKey:str_section]count];
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CustomCellTableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:@"customCell" forIndexPath:indexPath];
    
    // Configure the cell...
    if (_buscaHero.active) {
        
        cell.myHero = _resultHeroes[indexPath.row];
        
    }else {
        
        cell.myHero = _heroes[indexPath.row];
        
        NSString *str_section = [_heroKeys objectAtIndex:indexPath.section];
        Hero *hero = [[_heroIndex objectForKey:str_section]objectAtIndex:indexPath.row];
        cell.myHero = hero;
    }
    
    [cell configure];
    return cell;
}

-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    if(!_buscaHero.active)
        return _heroKeys;
    
    return nil;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    //1. Setup the CATransform3D structure
    CATransform3D rotation;
    rotation = CATransform3DMakeRotation( 0.0, 0.0, (90.0*M_PI)/180, 0.0);
    rotation.m33 = 1.0/ -100;
    
    
    //2. Define the initial state (Before the animation)
    cell.layer.shadowColor = [[UIColor blackColor]CGColor];
    cell.layer.shadowOffset = CGSizeMake(10, 10);
    cell.alpha = 0;
    
    cell.layer.transform = rotation;
    cell.layer.anchorPoint = CGPointMake(0.5, 0.5);
    
    
    //3. Define the final state (After the animation) and commit the animation
    [UIView beginAnimations:@"rotation" context:NULL];
    [UIView setAnimationDuration:0.5];
    cell.layer.transform = CATransform3DIdentity;
    cell.alpha = 1;
    cell.layer.shadowOffset = CGSizeMake(0, 0);
    [UIView commitAnimations];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _heroAtual = indexPath.row;
    
//    [tableView deselectRowAtIndexPath:indexPath animated:true];
    
    Hero *hero;
    
    if(_buscaHero.active)
    {
        hero = [_resultHeroes objectAtIndex:indexPath.row];
        [[[DataManager getDataManager]dict]setObject:hero forKey:@"showHero"];
    }
    else
    {
        
        NSString *str_section = [_heroKeys objectAtIndex:indexPath.section];
        hero = [[_heroIndex objectForKey:str_section]objectAtIndex:indexPath.row];
        [[[DataManager getDataManager]dict]setObject:hero forKey:@"showHero"];
    }
    
    PerfilViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"perfil"];
    
    vc.hero = hero;
    
    [self dismissViewControllerAnimated:NO completion:nil];
    
    [self presentViewController:vc animated:YES completion:nil];
}


#pragma mark UISearchBarDelegate

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self resignFirstResponder];
}

#pragma mark UISearchResulsUpdating

-(void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    NSString *textoBusca = _buscaHero.searchBar.text;
    [self buscar:textoBusca];
    [[self tableView]reloadData];
}
/*

#pragma mark navigation

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    Hero *hero;
    if(_buscaHero.active)
    {
        hero = [_resultHeroes objectAtIndex:_heroAtual];
        [[[DataManager getDataManager]dict]setObject:hero forKey:@"showHero"];
    }
    else
    {
        hero = [_heroes objectAtIndex:_heroAtual];
        [[[DataManager getDataManager]dict]setObject:hero forKey:@"showHero"];
    }
}
 */

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
