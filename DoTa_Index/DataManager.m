//
//  DataManager.m
//  Quizz
//
//  Created by Andre Ferreira dos Santos on 17/03/15.
//  Copyright (c) 2015 Andre Ferreira dos Santos. All rights reserved.
//

#import "DataManager.h"

@implementation DataManager

+(id)getDataManager
{
    static DataManager *datamanger;
    @synchronized(self)
    {
        if(datamanger == nil)
        {
            datamanger = [DataManager new];
        }
    }
    return datamanger;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _dict = [[NSMutableDictionary alloc]init];
    }
    return self;
}

-(NSString *)LoadStringFromFile:(NSString *)file useBundle:(BOOL)bundle{
    
    
    NSString *documentPath;
    if(bundle){
        documentPath = [[NSBundle mainBundle].bundlePath stringByAppendingPathComponent:file];
    }else{
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        documentPath = [[paths objectAtIndex:0]stringByAppendingPathComponent:file];
    }
    
    NSError *error;
    
    NSString *contentOfFile = [[NSString alloc] initWithContentsOfFile:documentPath encoding:NSUTF8StringEncoding error:&error];
    
    if(contentOfFile == nil){
        NSLog(@"Erro reading file:%@",[error description]);
    }
    
    return contentOfFile;
    
}

// essa funcao vai retornar um dicionario com uma chave (nome do heroi) armazenando um array com as informacoes dele
-(NSMutableArray *)getHeroes;
{
    NSMutableArray *res = [[NSMutableArray alloc] init];
    
    NSString *loaded = [self LoadStringFromFile:@"heroes.csv" useBundle:YES];
    
    //Me retorna um array com os blocos de herois, com suas informacoes
    
    NSArray *heroes = [loaded componentsSeparatedByString:@"\n"];
    
    NSUInteger n_heroes = [heroes count];
    
    for(NSUInteger i=0;i<(n_heroes -1);i++)
    {
        NSArray *hero = [[heroes objectAtIndex:i] componentsSeparatedByString:@";"];
        
        /*
        NSArray *hero = @[
            [block objectAtIndex:0],  //Nome
            [block objectAtIndex:1],  //baseStr
            [block objectAtIndex:2],  //strGrow
            [block objectAtIndex:3],  //baseAgi
            [block objectAtIndex:4],  //agiGrow
            [block objectAtIndex:5],  //baseInt
            [block objectAtIndex:6],  //intGrow
            [block objectAtIndex:7],  //movSpeed
            [block objectAtIndex:8],  //atkRange
            [block objectAtIndex:9],  //colSize
            [block objectAtIndex:10], //img_Path
        ];*/
        
        [res addObject:hero];
    }
    
    
    return res;
}


@end
