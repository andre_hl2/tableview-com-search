//
//  Hero.m
//  DoTa_Index
//
//  Created by Andre Ferreira dos Santos on 25/03/15.
//  Copyright (c) 2015 Andre Ferreira dos Santos. All rights reserved.
//

#import "Hero.h"

@implementation Hero

-(void)configure:(NSMutableArray *)ref
{
    _nome = [ref objectAtIndex:0];
    _str = [ref objectAtIndex:1];
    _strGrow = [ref objectAtIndex:2];
    _agi = [ref objectAtIndex:3];
    _agiGrow = [ref objectAtIndex:4];
    _inte = [ref objectAtIndex:5];
    _inteGrow = [ref objectAtIndex:6];
    _moveSpeed = [ref objectAtIndex:7];
    _atkRange = [ref objectAtIndex:8];
    _colSize = [ref objectAtIndex:9];
    _img_Path = [ref objectAtIndex:10];
    
}

@end
