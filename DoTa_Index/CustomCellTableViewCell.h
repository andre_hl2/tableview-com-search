//
//  CustomCellTableViewCell.h
//  DoTa_Index
//
//  Created by Andre Ferreira dos Santos on 24/03/15.
//  Copyright (c) 2015 Andre Ferreira dos Santos. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Hero.h"

@interface CustomCellTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelHeroName;
@property (weak, nonatomic) IBOutlet UIImageView *imageHero;

@property Hero *myHero;

-(void)configure;

@end
