//
//  PerfilViewController.h
//  DoTa_Index
//
//  Created by Andre Ferreira dos Santos on 24/03/15.
//  Copyright (c) 2015 Andre Ferreira dos Santos. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Hero.h"

@interface PerfilViewController : UIViewController


@property Hero *hero;

@property NSArray *heroes;
@property int row;

//outlets
@property (weak, nonatomic) IBOutlet UILabel *heroName;
@property (weak, nonatomic) IBOutlet UIImageView *heroImage;

@property (weak, nonatomic) IBOutlet UILabel *labStr;
@property (weak, nonatomic) IBOutlet UILabel *labAgi;
@property (weak, nonatomic) IBOutlet UILabel *labInt;

@property (weak, nonatomic) IBOutlet UILabel *labatkRange;
@property (weak, nonatomic) IBOutlet UILabel *labColSize;

//acoes de botoes
- (IBAction)voltar:(id)sender;

@end
